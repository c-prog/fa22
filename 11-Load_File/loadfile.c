#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s file_to_load\n", argv[0]);
        exit(1);
    }

    struct stat fileinfo;
    stat(argv[1], &fileinfo);
    printf("File size is: %ld\n", fileinfo.st_size);

    char *contents = malloc(fileinfo.st_size);

    FILE *f = fopen(argv[1], "r");
    if (!f)
    {
        char errmsg[100];
        sprintf(errmsg, "Can't open %s for reading", argv[1]);
        perror(errmsg);
        exit(1);
    }

    fread(contents, 1, fileinfo.st_size, f);
    fclose(f);

    // Replace newline with nulls.
    // Keep count as we go. This will give us the number of 
    // entries in the file.
    int num_entries = 0;
    for (int i = 0; i < fileinfo.st_size; i++)
    {
        if (contents[i] == '\n') {
            contents[i] = '\0';
            num_entries++;
        }
    }

    // Allocate space for the array of pointers
    char ** entries = malloc(num_entries * sizeof(char *));

    // Connect the array of pointers to each entry in the file
    entries[0] = &contents[0];
    int e = 1;
    for (int i = 1; i < fileinfo.st_size - 1; i++)
    {
        if (contents[i] == '\0')
        {
            entries[e] = &contents[i+1];
            e++;
        }
    }

    // Print out each string in the file
    //for (int i = 0; i < e; i++)
    //{
        //printf("%d %s\n", i, entries[i]);
    //}

    char target[50];
    printf("Search for: ");
    scanf("%s", target);

    for (int i = 0; i < e; i++)
    {
        if (strcmp(target, entries[i]) == 0)
        {
            printf("Found it on line %d\n", i);
        }
    }

}